import AppContext from './AppContext';

abstract class AbstractMaterial {
    // protected appContext: AppContext;
    public constructor(protected appContext: AppContext) {

    }
}

export default AbstractMaterial;
